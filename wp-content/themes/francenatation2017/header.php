<?php
/*
 * @package WordPress
 * @subpackage francenatation2017.fr
 */
?>
    <!DOCTYPE html>
    <!--[if lt IE 7]>      <html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
    <!--[if IE 7]>         <html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"> <![endif]-->
    <!--[if IE 8]>         <html <?php language_attributes(); ?> class="no-js lt-ie9"> <![endif]-->
    <!--[if gt IE 8]><!-->
    <html <?php language_attributes(); ?> class="no-js">
    <!--<![endif]-->

    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

        <?php echo wp_site_icon(); ?>
            <?php wp_head(); ?>



    </head>

    <body <?php body_class(); ?>>

        <!-- PRELOADING  -->
        <div id="preload">
            <div class="preload">
                <div class="loader">
                    Loading...
                </div>
            </div>
        </div>

        <!-- NAVIGATION -->
        <nav class="navbar navbar-fixed-top navbar-custom" role="navigation">
            <a class="navbar-logo" data-scroll href="#splash">
                <div class="navbar-logo_full">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/home__logo.png" alt="">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/home__logo-secondary.png" alt="">
                </div>
                <div class="navbar-logo_small">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/home__logo_small.png" alt="">
                </div>
            </a>
            <ul class="nav navbar-nav" data-scroll-header>
                <li><a data-scroll href="#evenement">L'événement</a></li>
                <li><a data-scroll href="#actualites">Actualités</a></li>
                <li><a data-scroll href="#programme">Programme &amp; Résultats</a></li>
                <li><a data-scroll href="#billetterie">Billetterie</a></li>
                <li><a data-scroll href="#partenaire">Partenaires</a></li>
                <li><a data-scroll href="#infos">Infos pratiques</a></li>
                <li><a data-scroll href="#comite">Comité d'organisation</a></li>
                <li><a data-scroll href="#contact">Contact</a></li>
                <li class="navbar-overflow">
                    <span class="navbar-overflow__button"><i class="fa fa-plus"></i></span>
                    <ul class="navbar-overflow__list"></ul>
                </li>
            </ul>
            <div class="navbar-facebook">
                <a href=""><i class="fa fa-facebook" aria-disabled="true"></i> Retrouvez-nous sur Facebook</a>
            </div>
        </nav>

        <section id="splash" class="splash">
            <span class="background_01"></span>
            <span class="background_02" data-stellar-ratio="1.5"></span>
            <span class="background_03"></span>
            <div class="splash__intro">
                <h1 class="splash__intro__name"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/home__title.png" alt="Championnats de France Natation Strasbourg"></h1>
                <div class="splash__intro__date">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/home__date.png" alt="23 au 28 Mai">
                </div>

                <div id="countdown" class="splash__intro__countdown"></div>
            </div>
        </section>

        <section id="evenement" class="event">
            <div class="event__intro">
                <h2 class="uppercase">À propos de l'événement</h2>
                <p class="lead">Pendant six jours les meilleurs nageurs français vont s’affronter pour décrocher leurs billets pour les championnats du monde de Budapest qui se dérouleront cet été. Vibrez en suivant les exploits de Camille Lacourt, Charlotte Bonnet, Jérémy Stravius et toute l’élite de la natation française.</p>
            </div>

            <div id="milestone" class="event__milestones">
                <div class="milestone">
                    <p class="timer" data-to="1946" data-speed="2000"></p>
                    <i class="fa fa-2x fa-ticket" aria-disabled="true"></i>
                    <p>places</p>

                </div>

                <div class="milestone">
                    <p class="timer" data-to="40" data-speed="2000"></p>
                    <i class="fa fa-2x fa-trophy" aria-disabled="true"></i>
                    <p>épreuves<br />dont <span>6</span> relais</p>
                </div>
                
                <div class="milestone">
                    <p class="timer" data-to="30" data-speed="2000"></p>
                    <i class="fa fa-2x fa-bullhorn" aria-disabled="true"></i>
                    <p>médias présents</p>
                </div>
            </div>
        </section>

        <section id="programme" class="program">
            <h2 class="uppercase text-center">Programme &amp; résultats</h2>   
            <div class="program__body">
                <div id="days" class="owl-carousel program__days">
                            <div class="program__day item uppercase">
                                <h4 class="day__date uppercase hidden-xs">23 Mai</h4>
                                <div class="day__weekday">Mardi</div>
                            </div>

                            <div class="program__day item uppercase">
                                <h4 class="day__date uppercase hidden-xs">24 mai</h4>
                                <div class="day__weekday">Mercredi</div>
                            </div>

                            <div class="program__day item uppercase">
                                <h4 class="day__date uppercase hidden-xs">25 mai</h4>
                                <div class="day__weekday">Jeudi</div>
                            </div>

                            <div class="program__day item uppercase">
                                <h4 class="day__date uppercase hidden-xs">26 mai</h4>
                                <div class="day__weekday">Vendredi</div>
                            </div>
                            
                            <div class="program__day item uppercase">
                                <h4 class="day__date uppercase hidden-xs">27 mai</h4>
                                <div class="day__weekday">Samedi</div>
                            </div>
                            
                            <div class="program__day item uppercase">
                                <h4 class="day__date uppercase hidden-xs">28 mai</h4>
                                <div class="day__weekday">Dimanche</div>
                            </div>
                        </div>
                <div id="timetable" class="owl-carousel owl-theme program__program-grid">
                    <div id="day1" class="program-grid__day">
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">09:30</span>
                            </div>

                            <div class="description">
                                <p>800m nage libre dames (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">10:00</span>
                            </div>

                            <div class="description">
                                <p>400m nage libre messieurs (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">10:30</span>
                            </div>

                            <div class="description">
                                <p>200m dos dames (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">11:00</span>
                            </div>

                            <div class="description">
                                <p>200m dos messieurs (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">11:30</span>
                            </div>

                            <div class="description">
                                <p>200m brasse dames (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">12:00</span>
                            </div>

                            <div class="description">
                                <p>200m brasse messieurs (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">13:30</span>
                            </div>

                            <div class="description">
                                <p>200m papillon dames (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">14:00</span>
                            </div>

                            <div class="description">
                                <p>200m papillon messieurs (séries)</p>
                            </div>
                        </div>
                    </div>
                    <div id="day2" class="program-grid__day">
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">09:30</span>
                            </div>

                            <div class="description">
                                <p>800m nage libre dames (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">10:00</span>
                            </div>

                            <div class="description">
                                <p>400m nage libre messieurs (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">10:30</span>
                            </div>

                            <div class="description">
                                <p>200m dos dames (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">11:00</span>
                            </div>

                            <div class="description">
                                <p>200m dos messieurs (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">11:30</span>
                            </div>

                            <div class="description">
                                <p>200m brasse dames (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">12:00</span>
                            </div>

                            <div class="description">
                                <p>200m brasse messieurs (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">13:30</span>
                            </div>

                            <div class="description">
                                <p>200m papillon dames (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">14:00</span>
                            </div>

                            <div class="description">
                                <p>200m papillon messieurs (séries)</p>
                            </div>
                        </div>
                    </div>
                    <div id="day3" class="program-grid__day">
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">09:30</span>
                            </div>

                            <div class="description">
                                <p>800m nage libre dames (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">10:00</span>
                            </div>

                            <div class="description">
                                <p>400m nage libre messieurs (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">10:30</span>
                            </div>

                            <div class="description">
                                <p>200m dos dames (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">11:00</span>
                            </div>

                            <div class="description">
                                <p>200m dos messieurs (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">11:30</span>
                            </div>

                            <div class="description">
                                <p>200m brasse dames (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">12:00</span>
                            </div>

                            <div class="description">
                                <p>200m brasse messieurs (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">13:30</span>
                            </div>

                            <div class="description">
                                <p>200m papillon dames (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">14:00</span>
                            </div>

                            <div class="description">
                                <p>200m papillon messieurs (séries)</p>
                            </div>
                        </div>
                    </div>
                    <div id="day4" class="program-grid__day">
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">09:30</span>
                            </div>

                            <div class="description">
                                <p>800m nage libre dames (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">10:00</span>
                            </div>

                            <div class="description">
                                <p>400m nage libre messieurs (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">10:30</span>
                            </div>

                            <div class="description">
                                <p>200m dos dames (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">11:00</span>
                            </div>

                            <div class="description">
                                <p>200m dos messieurs (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">11:30</span>
                            </div>

                            <div class="description">
                                <p>200m brasse dames (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">12:00</span>
                            </div>

                            <div class="description">
                                <p>200m brasse messieurs (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">13:30</span>
                            </div>

                            <div class="description">
                                <p>200m papillon dames (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">14:00</span>
                            </div>

                            <div class="description">
                                <p>200m papillon messieurs (séries)</p>
                            </div>
                        </div>
                    </div>
                    <div id="day5" class="program-grid__day">
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">09:30</span>
                            </div>

                            <div class="description">
                                <p>800m nage libre dames (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">10:00</span>
                            </div>

                            <div class="description">
                                <p>400m nage libre messieurs (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">10:30</span>
                            </div>

                            <div class="description">
                                <p>200m dos dames (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">11:00</span>
                            </div>

                            <div class="description">
                                <p>200m dos messieurs (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">11:30</span>
                            </div>

                            <div class="description">
                                <p>200m brasse dames (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">12:00</span>
                            </div>

                            <div class="description">
                                <p>200m brasse messieurs (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">13:30</span>
                            </div>

                            <div class="description">
                                <p>200m papillon dames (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">14:00</span>
                            </div>

                            <div class="description">
                                <p>200m papillon messieurs (séries)</p>
                            </div>
                        </div>
                    </div>
                    <div id="day6" class="program-grid__day">
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">09:30</span>
                            </div>

                            <div class="description">
                                <p>800m nage libre dames (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">10:00</span>
                            </div>

                            <div class="description">
                                <p>400m nage libre messieurs (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">10:30</span>
                            </div>

                            <div class="description">
                                <p>200m dos dames (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">11:00</span>
                            </div>

                            <div class="description">
                                <p>200m dos messieurs (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">11:30</span>
                            </div>

                            <div class="description">
                                <p>200m brasse dames (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">12:00</span>
                            </div>

                            <div class="description">
                                <p>200m brasse messieurs (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">13:30</span>
                            </div>

                            <div class="description">
                                <p>200m papillon dames (séries)</p>
                            </div>
                        </div>
                        <div class="program-grid__event">
                            <div class="icon">
                                <span class="time">14:00</span>
                            </div>

                            <div class="description">
                                <p>200m papillon messieurs (séries)</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="billetterie" class="purchase">
            <h2 class="uppercase text-center">Billetterie</h2>
            <p class="lead">Ouverture de la billetterie le <span>21 mars</span> à <span>12h00</span>.</p>
        </section>

        <section id="partenaire" class="sponsors">
            <h2 class="uppercase">Nos partenaires</h2>
            <div class="sponsor-slider">
                <div class="sponsors__img sponsors__img sponsors__img_big">
                    <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/sponsor__eurometropole.png" alt="Eurométropole de Strasbourg" />
                </div>
                <div class="sponsors__img sponsors__img sponsors__img_big">
                    <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/sponsor__grandest.png" alt="Région Grand Est" />
                </div>
            </div>
            <div class="sponsor-slider">
                <div class="sponsors__img sponsors__img_small shadowed">
                    <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/sponsor__edf.png" alt="EDF" />
                </div>
                <div class="sponsors__img sponsors__img_small">
                    <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/sponsor__beinsports.png" alt="BeinSports" />
                </div>
                <div class="sponsors__img sponsors__img_small shadowed">
                    <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/sponsor__campanile.png" alt="Campanile" />
                </div>
                <div class="sponsors__img sponsors__img_small">
                    <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/sponsor__directmatin.png" alt="Direct Matin" />
                </div>
                <div class="sponsors__img sponsors__img_small">
                    <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/sponsor__ffn.png" alt="ffn.png" />
                </div>
                <div class="sponsors__img sponsors__img_small shadowed">
                    <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/sponsor__handisport.png" alt="HandiSport" />
                </div>
                <div class="sponsors__img sponsors__img_small shadowed">
                    <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/sponsor__mds.png" alt="MDS" />
                </div>
                <div class="sponsors__img sponsors__img_small">
                    <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/sponsor__rmc.png" alt="RMC" />
                </div>
                <div class="sponsors__img sponsors__img_small shadowed">
                    <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/sponsor__tyr.png" alt="Tyr" />
                </div>
            </div>

            <p class="lead uppercase">Devenir partenaire</p>
            <p>Découvrez nos formules pour devenir partenaire et profitez de cet évènement majeur pour communiquer et partager auprès de vos clients.</p>
            <a class="button button-big button-dark" href="#contact">Télécharger la brochure</a>
        </section>

        <section id="infos" class="infos">
            <div class="infos__venue">
                <!-- map -->
                <div id="gmap_canvas" class="map"></div>
                <div class="venue__address">
                    <h2 class="uppercase">Centre nautique<br />de Schiltigheim</h2>

                    <p class="address">
                        <i class="fa fa-2x fa-map-marker fa-inverse"></i> 
                        9 Rue de Turenne<br>
                        67300 Schiltigheim
                    </p>
                </div>
            </div>
            <div class="infos__faq">
                <div class="panel-group infos__faq__container">
                    <h2 class="uppercase">Informations pratiques</h2>
                    <p>Suspendisse mattis tempus erat, ac aliquam lectus</p>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#faq" href="#collapseOne" class="collapsed">
                                  <i class="fa fa-2x fa-caret-down"></i> <span>Mauris id vestibulum eros</span>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" style="height: 0px;">
                            <div class="panel-body">
                                <p class="small">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#faq" href="#collapseTwo" class="collapsed">
                                  <i class="fa fa-2x fa-caret-down"></i> <span>Phasellus fermentum dolor commodo</span>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="small">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#faq" href="#collapseThree" class="collapsed">
                                  <i class="fa fa-2x fa-caret-down"></i> <span>Etiam nec sem mollis</span>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="small">Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#faq" href="#collapseFour" class="collapsed">
                                  <i class="fa fa-2x fa-caret-down"></i> <span>Etiam suscipit a velit volutpat dictum</span>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="small"> Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#faq" href="#collapseFive" class="collapsed">
                                  <i class="fa fa-2x fa-caret-down"></i> <span>Nulla quis adipiscing nisi</span>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFive" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="small">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="comite" class="comity">
            <h2 class="uppercase">Comité d'organisation</h2>
            <div class="comity__body">
                <div class="comity__person">
                    <h4>Personne 1</h4>
                    <span class="title">Titre</span>
                </div>
                <div class="comity__person">
                    <h4>Personne 2</h4>
                    <span class="title">Titre</span>
                </div>
                <div class="comity__person">
                    <h4>Personne 3</h4>
                    <span class="title">Titre</span>
                </div>
                <div class="comity__person">
                    <h4>Personne 4</h4>
                    <span class="title">Titre</span>
                </div>
                <div class="comity__person">
                    <h4>Personne 5</h4>
                    <span class="title">Titre</span>
                </div>
                <div class="comity__person">
                    <h4>Personne 6</h4>
                    <span class="title">Titre</span>
                </div>
                <div class="comity__person">
                    <h4>Personne 7</h4>
                    <span class="title">Titre</span>
                </div>
                <div class="comity__person">
                    <h4>Personne 8</h4>
                    <span class="title">Titre</span>
                </div>
            </div>
        </section>

        <section id="contact" class="contact">
            <div class="contact__block">
                <h2 class="uppercase">contact</h2>

                <div class="form">
                    <div class="control-group">
                        <div class="controls">
                            <label for="name">Nom</label><br>
                            <input type="text" name="name" id="name" required="" data-validation-required-message="Please enter your name">
                        </div>
                    </div>

                    <div class="control-group">
                        <div class="controls ">                           
                            <label for="email">Email</label><br>
                            <input type="email" name="email" id="email" required="" data-validation-required-message="Please enter your email">
                        </div>
                    </div>

                     <div class="control-group">
                        <div class="controls ">                            
                            <label for="message">Message</label><br>
                            <textarea name="message" id="message"></textarea>
                        </div>
                    </div>

                    <div class="form__button">
                        <button class="button button-dark">Envoyer</button>
                    </div>
                </div>

                <div class="social">
                    <a href="#">
                        <span class="fa-stack fa-lg">
                                        <i class="fa fa-circle fa-stack-2x"></i>
                                        <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                                    </span>
                    </a>

                    <a href="#">
                        <span class="fa-stack fa-lg">
                                        <i class="fa fa-circle fa-stack-2x"></i>
                                        <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                                    </span>
                    </a>

                    <a href="#">
                        <span class="fa-stack fa-lg">
                                        <i class="fa fa-circle fa-stack-2x"></i>
                                        <i class="fa fa-instagram fa-stack-1x fa-inverse"></i>
                                    </span>
                    </a>
                </div>

            </div>
        </section>