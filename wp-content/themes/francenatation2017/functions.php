<?php
// Nettoyage du <head> ------------------------------------------------

    function removeHeadLinks() {
        remove_action('wp_head', 'rsd_link');
        remove_action('wp_head', 'wlwmanifest_link');
    }
    add_action('init', 'removeHeadLinks');
    remove_action('wp_head', 'wp_generator');

//  Script no-js / js class
    function alx_html_js_class () {
        echo '<script>document.documentElement.className = document.documentElement.className.replace("no-js","js");</script>'. "\n";
    }
    add_action( 'wp_head', 'alx_html_js_class', 1 );
    
    function remove_admin_login_header() {
        remove_action('wp_head', '_admin_bar_bump_cb');
    }
    add_action('get_header', 'remove_admin_login_header');
    
    add_theme_support( 'title-tag' );
// --------------------------------------------------------------------

// Traductions --------------------------------------------------------

    load_theme_textdomain( 'francenatation2017', TEMPLATEPATH . '/languages' );

    $locale = get_locale();
    $locale_file = TEMPLATEPATH . "/languages/$locale.php";
    if ( is_readable($locale_file) ) {
        require_once($locale_file);
    }

// --------------------------------------------------------------------

// Retrait des attributs width et height des miniatures d'images --

    function clean_img_width_height($string){
        return preg_replace('/\<(.*?)(width="(.*?)")(.*?)(height="(.*?)")(.*?)\>/i', '<$1$4$7>',$string);
    }

// --------------------------------------------------------------------


// Menus personnalisés ------------------------------------------------

    if (function_exists('register_nav_menus')) {
        register_nav_menus(array(
            'mainmenu' 	=> __( 'Menu principal', 'francenatation2017' ),
        )); 
    }

// --------------------------------------------------------------------

// Tailles d'images ---------------------------------------------------

    add_theme_support('post-thumbnails');
    //add_image_size('large-thumbnail', '', '');

// --------------------------------------------------------------------

// Scripts et styles --------------------------------------------------
    
    function cfn2017_scripts() {
        if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {
            
            wp_register_style( 'ie_html5shiv', get_template_directory_uri() . '/js/ie/html5shiv.js' );
            wp_register_style( 'ie_respond', get_template_directory_uri() . '/js/ie/respond.min.js' );
            
            wp_style_add_data( 'ie_html5shiv', 'conditional', 'lt IE 9' );
            wp_style_add_data( 'ie_respond', 'conditional', 'lt IE 9' );
            wp_register_script('lodash', '//cdnjs.cloudflare.com/ajax/libs/lodash.js/3.5.0/lodash.min.js');
            wp_register_script('googlemaps', '//maps.google.com/maps/api/js?key=AIzaSyBbmmzE2T-55LP0Nn-HAV0rIx_QEgmOKSI');
            wp_register_script('bootstrap', get_template_directory_uri() . '/assets/js/plugins/bootstrap.min.js');
            wp_register_script('countdown', get_template_directory_uri() . '/assets/js/plugins/jquery.countdown.js');
            wp_register_script('owl.carousel', get_template_directory_uri() . '/assets/js/plugins/owl.carousel.min.js');
            wp_register_script('magnific.popup', get_template_directory_uri() . '/assets/js/plugins/jquery.magnific-popup.min.js');
            wp_register_script('countTo', get_template_directory_uri() . '/assets/js/plugins/jquery.countTo.js');
            wp_register_script('flipcard', get_template_directory_uri() . '/assets/js/plugins/flipCard.js');
            wp_register_script('stellar', get_template_directory_uri() . '/assets/js/plugins/jquery.stellar.min.js');
            wp_register_script('smoothscroll', get_template_directory_uri() . '/assets/js/plugins/smooth-scroll.js');
            wp_register_script('retina', get_template_directory_uri() . '/assets/js/plugins/retina-1.1.0.min.js');
            wp_register_script('scripts', get_template_directory_uri() . '/assets/js/scripts.js');
            
            wp_enqueue_script('jquery');
            wp_enqueue_style( 'ie_html5shiv');
            wp_enqueue_style( 'ie_respond');

            wp_enqueue_script('lodash');
            wp_enqueue_script('googlemaps');
            wp_enqueue_script('bootstrap');
            wp_enqueue_script('countdown');
            wp_enqueue_script('owl.carousel');
            wp_enqueue_script('magnific.popup');
            wp_enqueue_script('countTo');
            wp_enqueue_script('flipcard');
            wp_enqueue_script('stellar');
            wp_enqueue_script('smoothscroll');
            //wp_enqueue_script('retina');
            wp_enqueue_script('scripts');
            
            
        }
    }
    add_action('wp_enqueue_scripts', 'cfn2017_scripts');

    function cfn2017_styles() {
        wp_register_style('style', get_template_directory_uri() . '/style.css', array(), '1.0', 'all');
        wp_enqueue_style('style');
        //wp_register_style('gfonts', 'https://fonts.googleapis.com/css?family=Alegreya+Sans:400,300,700', array(), '1.0', 'all');
        //wp_enqueue_style('gfonts');
    }
    add_action('wp_enqueue_scripts', 'cfn2017_styles');

    function cfn2017_admin_styles() {
        wp_register_style('admin-style', get_template_directory_uri() . '/admin-style.css', array(), '1.0', 'all');
        wp_enqueue_style('admin-style');
    }
    add_action('admin_enqueue_scripts', 'cfn2017_admin_styles');
    add_action('login_enqueue_scripts', 'cfn2017_admin_styles');


    add_editor_style();

// --------------------------------------------------------------------

// Page d'options du thème --------------------------------------------
require_once get_template_directory() . '/assets/admin/theme-options.php';
// --------------------------------------------------------------------


if( ! function_exists('redirectt2homepage') ) {
  function redirect2homepage()
  {
    return get_bloginfo('url');
  }
add_filter('login_redirect', 'redirect2homepage');
}
?>