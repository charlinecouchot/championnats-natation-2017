/**
 * scripts.js
 */

/*jshint unused:false,undef:false,strict:false */


jQuery(document).ready(function (jQuery) {
    'use strict';
    
    var PriorityNav = {

  // used multiple times, so saving.
  menu: jQuery(".navbar-nav"),
  allNavElements: jQuery(".navbar-nav > li:not('.navbar-overflow')"),

  init: function() {
    this.bindUIActions();
    this.setupMenu();
  },

  setupMenu: function() {
    
    // Checking top position of first item (sometimes changes)
    var firstPos = jQuery(".navbar-nav > li:first").position();
        
    // Empty collection in which to put menu items to move
    var wrappedElements = jQuery();

    // Used to snag the previous menu item in addition to ones that have wrapped
    var first = true;

    // Loop through all the nav items...
    PriorityNav.allNavElements.each(function(i) {
      
      var el = jQuery(this);

      // ...in which to find wrapped elements
      var pos = el.position();
                  
      if (pos.top !== firstPos.top) {

        // If element is wrapped, add it to set
        wrappedElements = wrappedElements.add(el);

        // Add the previous one too, if first
        /*if (first) {
          wrappedElements = wrappedElements.add(PriorityNav.allNavElements.eq(i-1));
          first = false;
        }*/
      }
    });
        
    if (wrappedElements.length) {
    
      // Clone set before altering
      var newSet = wrappedElements.clone();

      // Hide ones that we're moving
      wrappedElements.addClass("navbar-overflow__hidden");

      // Add wrapped elements to dropdown
      jQuery(".navbar-overflow__list").append(newSet);

      // Show new menu
      jQuery(".navbar-overflow").addClass("show-inline-block");

      // Make overflow visible again so dropdown can be seen.
      jQuery(".navbar-nav").css("overflow", "visible");

    }
  },

  tearDown: function() {
    jQuery(".navbar-overflow__list").empty();
    jQuery(".navbar-overflow").removeClass("show-inline-block");
    PriorityNav.allNavElements.removeClass("navbar-overflow__hidden");
  },

  bindUIActions: function() {
    jQuery(".navbar-overflow__button").on("click", function() {
      jQuery(".navbar-overflow__list").toggleClass("navbar-overflow__visible");
    });

    jQuery(window)
      .resize(function() {
        PriorityNav.menu.addClass("resizing");
      })
      .resize(_.debounce(function() {
        PriorityNav.tearDown();
        PriorityNav.setupMenu();
        PriorityNav.menu.removeClass("resizing");
      }, 500));
  }

};

PriorityNav.init();

    //SMOOTH SCROLL
    smoothScroll.init({
        selectorHeader: '[data-scroll-header]'
    });


    //COUNTDOWN TIMER
    jQuery('#countdown').countdown({
        until: new Date(2017, 5, 23),
        
    }); // enter event day

    //MILESTONE
    jQuery('.timer').countTo();


    jQuery('.register').magnificPopup({
        type: 'ajax'
    });

    //OWLCAROUSEL SCHEDULE
    var timetable = jQuery(".program__program-grid");
    var days = jQuery(".program__days");

    timetable.owlCarousel({
        items: 2,
        itemsMobile: [479, 1],
        slideSpeed: 1000,
        navigation: false,
        pagination: false,
        afterAction: syncPosition,
        responsiveRefreshRate: 200,
    });

    days.owlCarousel({
        items: 6,
        itemsMobile: [479, 4],
        pagination: false,
        responsiveRefreshRate: 100,
        afterInit: function (el) {
            el.find(".owl-item").eq(0).addClass("synced");
            el.find(".owl-item").eq(1).addClass("synced");
        }
    });

    function syncPosition(el) {
        var current = [this.currentItem, this.currentItem + 1];
        days.find(".owl-item").removeClass("synced").filter(function(index) {
            return current.indexOf(index) > -1;
        }).addClass("synced");
        if (days.data("owlCarousel") !== undefined) {
            center(current);
        }
    }

    days.on("click", ".owl-item", function (e) {
        e.preventDefault();
        var number = jQuery(this).data("owlItem");
        timetable.trigger("owl.goTo", number);
    });

    function center(number) {
        var daysvisible = days.data("owlCarousel").owl.visibleItems;
        var num = number;
        var found = false;
        for (var i in daysvisible) {
            if (num === daysvisible[i]) {
                found = true;
            }
        }

        if (found === false) {
            if (num > daysvisible[daysvisible.length - 1]) {
                days.trigger("owl.goTo", num - daysvisible.length + 2);
            } else {
                if (num - 1 === -1) {
                    num = 0;
                }
                days.trigger("owl.goTo", num);
            }
        } else if (num === daysvisible[daysvisible.length - 1]) {
            days.trigger("owl.goTo", daysvisible[1]);
        } else if (num === daysvisible[0]) {
            days.trigger("owl.goTo", num - 1);
        }

    }

    //FIX HOVER EFFECT ON IOS DEVICES
    document.addEventListener("touchstart", function () {}, true);
});

jQuery(window).load(function () {
    
    
    //PARALLAX BACKGROUND
    jQuery(window).resize(function() {
        var windowHeight = jQuery(this).height();
        jQuery('#splash').css('height', windowHeight);
    }).resize();
    
    jQuery(window).stellar({
        horizontalScrolling: false,
    });


    //PRELOADER
    jQuery('#preload').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.


    //HEADER ANIMATION
    jQuery(window).scroll(function () {
        if (jQuery("body").hasClass("admin-bar")) {
            if (jQuery(".navbar").offset().top > 55) {
                jQuery(".navbar-fixed-top").addClass("top-nav-collapse");
            } else {
                jQuery(".navbar-fixed-top").removeClass("top-nav-collapse");
            }
        } else {
            if (jQuery(".navbar").offset().top > 50) {
                jQuery(".navbar-fixed-top").addClass("top-nav-collapse");
            } else {
                jQuery(".navbar-fixed-top").removeClass("top-nav-collapse");
            }
        }
    });

});

//GOOGLE MAP
function init_map() {
    var myOptions = {
        zoom: 17,
        center: new google.maps.LatLng(48.612221, 7.729118), //change the coordinates
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false,
        styles: [{
            featureType: 'all',
            stylers: [{
                saturation: -100
            }, {
                gamma: 0.50
            }]
        }]
    };
    map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions);
    marker = new google.maps.Marker({
        map: map,
        position: new google.maps.LatLng(48.612221, 7.729118) //change the coordinates
    });
}
google.maps.event.addDomListener(window, 'load', init_map);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImN1c3RvbS5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6InNjcmlwdHMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIHNjcmlwdHMuanNcbiAqL1xuXG4vKmpzaGludCB1bnVzZWQ6ZmFsc2UsdW5kZWY6ZmFsc2Usc3RyaWN0OmZhbHNlICovXG5cblxualF1ZXJ5KGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoalF1ZXJ5KSB7XG4gICAgJ3VzZSBzdHJpY3QnO1xuICAgIFxuICAgIHZhciBQcmlvcml0eU5hdiA9IHtcblxuICAvLyB1c2VkIG11bHRpcGxlIHRpbWVzLCBzbyBzYXZpbmcuXG4gIG1lbnU6IGpRdWVyeShcIi5uYXZiYXItbmF2XCIpLFxuICBhbGxOYXZFbGVtZW50czogalF1ZXJ5KFwiLm5hdmJhci1uYXYgPiBsaTpub3QoJy5uYXZiYXItb3ZlcmZsb3cnKVwiKSxcblxuICBpbml0OiBmdW5jdGlvbigpIHtcbiAgICB0aGlzLmJpbmRVSUFjdGlvbnMoKTtcbiAgICB0aGlzLnNldHVwTWVudSgpO1xuICB9LFxuXG4gIHNldHVwTWVudTogZnVuY3Rpb24oKSB7XG4gICAgXG4gICAgLy8gQ2hlY2tpbmcgdG9wIHBvc2l0aW9uIG9mIGZpcnN0IGl0ZW0gKHNvbWV0aW1lcyBjaGFuZ2VzKVxuICAgIHZhciBmaXJzdFBvcyA9IGpRdWVyeShcIi5uYXZiYXItbmF2ID4gbGk6Zmlyc3RcIikucG9zaXRpb24oKTtcbiAgICAgICAgXG4gICAgLy8gRW1wdHkgY29sbGVjdGlvbiBpbiB3aGljaCB0byBwdXQgbWVudSBpdGVtcyB0byBtb3ZlXG4gICAgdmFyIHdyYXBwZWRFbGVtZW50cyA9IGpRdWVyeSgpO1xuXG4gICAgLy8gVXNlZCB0byBzbmFnIHRoZSBwcmV2aW91cyBtZW51IGl0ZW0gaW4gYWRkaXRpb24gdG8gb25lcyB0aGF0IGhhdmUgd3JhcHBlZFxuICAgIHZhciBmaXJzdCA9IHRydWU7XG5cbiAgICAvLyBMb29wIHRocm91Z2ggYWxsIHRoZSBuYXYgaXRlbXMuLi5cbiAgICBQcmlvcml0eU5hdi5hbGxOYXZFbGVtZW50cy5lYWNoKGZ1bmN0aW9uKGkpIHtcbiAgICAgIFxuICAgICAgdmFyIGVsID0galF1ZXJ5KHRoaXMpO1xuXG4gICAgICAvLyAuLi5pbiB3aGljaCB0byBmaW5kIHdyYXBwZWQgZWxlbWVudHNcbiAgICAgIHZhciBwb3MgPSBlbC5wb3NpdGlvbigpO1xuICAgICAgICAgICAgICAgICAgXG4gICAgICBpZiAocG9zLnRvcCAhPT0gZmlyc3RQb3MudG9wKSB7XG5cbiAgICAgICAgLy8gSWYgZWxlbWVudCBpcyB3cmFwcGVkLCBhZGQgaXQgdG8gc2V0XG4gICAgICAgIHdyYXBwZWRFbGVtZW50cyA9IHdyYXBwZWRFbGVtZW50cy5hZGQoZWwpO1xuXG4gICAgICAgIC8vIEFkZCB0aGUgcHJldmlvdXMgb25lIHRvbywgaWYgZmlyc3RcbiAgICAgICAgLyppZiAoZmlyc3QpIHtcbiAgICAgICAgICB3cmFwcGVkRWxlbWVudHMgPSB3cmFwcGVkRWxlbWVudHMuYWRkKFByaW9yaXR5TmF2LmFsbE5hdkVsZW1lbnRzLmVxKGktMSkpO1xuICAgICAgICAgIGZpcnN0ID0gZmFsc2U7XG4gICAgICAgIH0qL1xuICAgICAgfVxuICAgIH0pO1xuICAgICAgICBcbiAgICBpZiAod3JhcHBlZEVsZW1lbnRzLmxlbmd0aCkge1xuICAgIFxuICAgICAgLy8gQ2xvbmUgc2V0IGJlZm9yZSBhbHRlcmluZ1xuICAgICAgdmFyIG5ld1NldCA9IHdyYXBwZWRFbGVtZW50cy5jbG9uZSgpO1xuXG4gICAgICAvLyBIaWRlIG9uZXMgdGhhdCB3ZSdyZSBtb3ZpbmdcbiAgICAgIHdyYXBwZWRFbGVtZW50cy5hZGRDbGFzcyhcIm5hdmJhci1vdmVyZmxvd19faGlkZGVuXCIpO1xuXG4gICAgICAvLyBBZGQgd3JhcHBlZCBlbGVtZW50cyB0byBkcm9wZG93blxuICAgICAgalF1ZXJ5KFwiLm5hdmJhci1vdmVyZmxvd19fbGlzdFwiKS5hcHBlbmQobmV3U2V0KTtcblxuICAgICAgLy8gU2hvdyBuZXcgbWVudVxuICAgICAgalF1ZXJ5KFwiLm5hdmJhci1vdmVyZmxvd1wiKS5hZGRDbGFzcyhcInNob3ctaW5saW5lLWJsb2NrXCIpO1xuXG4gICAgICAvLyBNYWtlIG92ZXJmbG93IHZpc2libGUgYWdhaW4gc28gZHJvcGRvd24gY2FuIGJlIHNlZW4uXG4gICAgICBqUXVlcnkoXCIubmF2YmFyLW5hdlwiKS5jc3MoXCJvdmVyZmxvd1wiLCBcInZpc2libGVcIik7XG5cbiAgICB9XG4gIH0sXG5cbiAgdGVhckRvd246IGZ1bmN0aW9uKCkge1xuICAgIGpRdWVyeShcIi5uYXZiYXItb3ZlcmZsb3dfX2xpc3RcIikuZW1wdHkoKTtcbiAgICBqUXVlcnkoXCIubmF2YmFyLW92ZXJmbG93XCIpLnJlbW92ZUNsYXNzKFwic2hvdy1pbmxpbmUtYmxvY2tcIik7XG4gICAgUHJpb3JpdHlOYXYuYWxsTmF2RWxlbWVudHMucmVtb3ZlQ2xhc3MoXCJuYXZiYXItb3ZlcmZsb3dfX2hpZGRlblwiKTtcbiAgfSxcblxuICBiaW5kVUlBY3Rpb25zOiBmdW5jdGlvbigpIHtcbiAgICBqUXVlcnkoXCIubmF2YmFyLW92ZXJmbG93X19idXR0b25cIikub24oXCJjbGlja1wiLCBmdW5jdGlvbigpIHtcbiAgICAgIGpRdWVyeShcIi5uYXZiYXItb3ZlcmZsb3dfX2xpc3RcIikudG9nZ2xlQ2xhc3MoXCJuYXZiYXItb3ZlcmZsb3dfX3Zpc2libGVcIik7XG4gICAgfSk7XG5cbiAgICBqUXVlcnkod2luZG93KVxuICAgICAgLnJlc2l6ZShmdW5jdGlvbigpIHtcbiAgICAgICAgUHJpb3JpdHlOYXYubWVudS5hZGRDbGFzcyhcInJlc2l6aW5nXCIpO1xuICAgICAgfSlcbiAgICAgIC5yZXNpemUoXy5kZWJvdW5jZShmdW5jdGlvbigpIHtcbiAgICAgICAgUHJpb3JpdHlOYXYudGVhckRvd24oKTtcbiAgICAgICAgUHJpb3JpdHlOYXYuc2V0dXBNZW51KCk7XG4gICAgICAgIFByaW9yaXR5TmF2Lm1lbnUucmVtb3ZlQ2xhc3MoXCJyZXNpemluZ1wiKTtcbiAgICAgIH0sIDUwMCkpO1xuICB9XG5cbn07XG5cblByaW9yaXR5TmF2LmluaXQoKTtcblxuICAgIC8vU01PT1RIIFNDUk9MTFxuICAgIHNtb290aFNjcm9sbC5pbml0KHtcbiAgICAgICAgc2VsZWN0b3JIZWFkZXI6ICdbZGF0YS1zY3JvbGwtaGVhZGVyXSdcbiAgICB9KTtcblxuXG4gICAgLy9DT1VOVERPV04gVElNRVJcbiAgICBqUXVlcnkoJyNjb3VudGRvd24nKS5jb3VudGRvd24oe1xuICAgICAgICB1bnRpbDogbmV3IERhdGUoMjAxNywgNSwgMjMpLFxuICAgICAgICBcbiAgICB9KTsgLy8gZW50ZXIgZXZlbnQgZGF5XG5cbiAgICAvL01JTEVTVE9ORVxuICAgIGpRdWVyeSgnLnRpbWVyJykuY291bnRUbygpO1xuXG5cbiAgICBqUXVlcnkoJy5yZWdpc3RlcicpLm1hZ25pZmljUG9wdXAoe1xuICAgICAgICB0eXBlOiAnYWpheCdcbiAgICB9KTtcblxuICAgIC8vT1dMQ0FST1VTRUwgU0NIRURVTEVcbiAgICB2YXIgdGltZXRhYmxlID0galF1ZXJ5KFwiLnByb2dyYW1fX3Byb2dyYW0tZ3JpZFwiKTtcbiAgICB2YXIgZGF5cyA9IGpRdWVyeShcIi5wcm9ncmFtX19kYXlzXCIpO1xuXG4gICAgdGltZXRhYmxlLm93bENhcm91c2VsKHtcbiAgICAgICAgaXRlbXM6IDIsXG4gICAgICAgIGl0ZW1zTW9iaWxlOiBbNDc5LCAxXSxcbiAgICAgICAgc2xpZGVTcGVlZDogMTAwMCxcbiAgICAgICAgbmF2aWdhdGlvbjogZmFsc2UsXG4gICAgICAgIHBhZ2luYXRpb246IGZhbHNlLFxuICAgICAgICBhZnRlckFjdGlvbjogc3luY1Bvc2l0aW9uLFxuICAgICAgICByZXNwb25zaXZlUmVmcmVzaFJhdGU6IDIwMCxcbiAgICB9KTtcblxuICAgIGRheXMub3dsQ2Fyb3VzZWwoe1xuICAgICAgICBpdGVtczogNixcbiAgICAgICAgaXRlbXNNb2JpbGU6IFs0NzksIDRdLFxuICAgICAgICBwYWdpbmF0aW9uOiBmYWxzZSxcbiAgICAgICAgcmVzcG9uc2l2ZVJlZnJlc2hSYXRlOiAxMDAsXG4gICAgICAgIGFmdGVySW5pdDogZnVuY3Rpb24gKGVsKSB7XG4gICAgICAgICAgICBlbC5maW5kKFwiLm93bC1pdGVtXCIpLmVxKDApLmFkZENsYXNzKFwic3luY2VkXCIpO1xuICAgICAgICAgICAgZWwuZmluZChcIi5vd2wtaXRlbVwiKS5lcSgxKS5hZGRDbGFzcyhcInN5bmNlZFwiKTtcbiAgICAgICAgfVxuICAgIH0pO1xuXG4gICAgZnVuY3Rpb24gc3luY1Bvc2l0aW9uKGVsKSB7XG4gICAgICAgIHZhciBjdXJyZW50ID0gW3RoaXMuY3VycmVudEl0ZW0sIHRoaXMuY3VycmVudEl0ZW0gKyAxXTtcbiAgICAgICAgZGF5cy5maW5kKFwiLm93bC1pdGVtXCIpLnJlbW92ZUNsYXNzKFwic3luY2VkXCIpLmZpbHRlcihmdW5jdGlvbihpbmRleCkge1xuICAgICAgICAgICAgcmV0dXJuIGN1cnJlbnQuaW5kZXhPZihpbmRleCkgPiAtMTtcbiAgICAgICAgfSkuYWRkQ2xhc3MoXCJzeW5jZWRcIik7XG4gICAgICAgIGlmIChkYXlzLmRhdGEoXCJvd2xDYXJvdXNlbFwiKSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBjZW50ZXIoY3VycmVudCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBkYXlzLm9uKFwiY2xpY2tcIiwgXCIub3dsLWl0ZW1cIiwgZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICB2YXIgbnVtYmVyID0galF1ZXJ5KHRoaXMpLmRhdGEoXCJvd2xJdGVtXCIpO1xuICAgICAgICB0aW1ldGFibGUudHJpZ2dlcihcIm93bC5nb1RvXCIsIG51bWJlcik7XG4gICAgfSk7XG5cbiAgICBmdW5jdGlvbiBjZW50ZXIobnVtYmVyKSB7XG4gICAgICAgIHZhciBkYXlzdmlzaWJsZSA9IGRheXMuZGF0YShcIm93bENhcm91c2VsXCIpLm93bC52aXNpYmxlSXRlbXM7XG4gICAgICAgIHZhciBudW0gPSBudW1iZXI7XG4gICAgICAgIHZhciBmb3VuZCA9IGZhbHNlO1xuICAgICAgICBmb3IgKHZhciBpIGluIGRheXN2aXNpYmxlKSB7XG4gICAgICAgICAgICBpZiAobnVtID09PSBkYXlzdmlzaWJsZVtpXSkge1xuICAgICAgICAgICAgICAgIGZvdW5kID0gdHJ1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChmb3VuZCA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgIGlmIChudW0gPiBkYXlzdmlzaWJsZVtkYXlzdmlzaWJsZS5sZW5ndGggLSAxXSkge1xuICAgICAgICAgICAgICAgIGRheXMudHJpZ2dlcihcIm93bC5nb1RvXCIsIG51bSAtIGRheXN2aXNpYmxlLmxlbmd0aCArIDIpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBpZiAobnVtIC0gMSA9PT0gLTEpIHtcbiAgICAgICAgICAgICAgICAgICAgbnVtID0gMDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZGF5cy50cmlnZ2VyKFwib3dsLmdvVG9cIiwgbnVtKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIGlmIChudW0gPT09IGRheXN2aXNpYmxlW2RheXN2aXNpYmxlLmxlbmd0aCAtIDFdKSB7XG4gICAgICAgICAgICBkYXlzLnRyaWdnZXIoXCJvd2wuZ29Ub1wiLCBkYXlzdmlzaWJsZVsxXSk7XG4gICAgICAgIH0gZWxzZSBpZiAobnVtID09PSBkYXlzdmlzaWJsZVswXSkge1xuICAgICAgICAgICAgZGF5cy50cmlnZ2VyKFwib3dsLmdvVG9cIiwgbnVtIC0gMSk7XG4gICAgICAgIH1cblxuICAgIH1cblxuICAgIC8vRklYIEhPVkVSIEVGRkVDVCBPTiBJT1MgREVWSUNFU1xuICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJ0b3VjaHN0YXJ0XCIsIGZ1bmN0aW9uICgpIHt9LCB0cnVlKTtcbn0pO1xuXG5qUXVlcnkod2luZG93KS5sb2FkKGZ1bmN0aW9uICgpIHtcbiAgICBcbiAgICBcbiAgICAvL1BBUkFMTEFYIEJBQ0tHUk9VTkRcbiAgICBqUXVlcnkod2luZG93KS5yZXNpemUoZnVuY3Rpb24oKSB7XG4gICAgICAgIHZhciB3aW5kb3dIZWlnaHQgPSBqUXVlcnkodGhpcykuaGVpZ2h0KCk7XG4gICAgICAgIGpRdWVyeSgnI3NwbGFzaCcpLmNzcygnaGVpZ2h0Jywgd2luZG93SGVpZ2h0KTtcbiAgICB9KS5yZXNpemUoKTtcbiAgICBcbiAgICBqUXVlcnkod2luZG93KS5zdGVsbGFyKHtcbiAgICAgICAgaG9yaXpvbnRhbFNjcm9sbGluZzogZmFsc2UsXG4gICAgfSk7XG5cblxuICAgIC8vUFJFTE9BREVSXG4gICAgalF1ZXJ5KCcjcHJlbG9hZCcpLmRlbGF5KDM1MCkuZmFkZU91dCgnc2xvdycpOyAvLyB3aWxsIGZhZGUgb3V0IHRoZSB3aGl0ZSBESVYgdGhhdCBjb3ZlcnMgdGhlIHdlYnNpdGUuXG5cblxuICAgIC8vSEVBREVSIEFOSU1BVElPTlxuICAgIGpRdWVyeSh3aW5kb3cpLnNjcm9sbChmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmIChqUXVlcnkoXCJib2R5XCIpLmhhc0NsYXNzKFwiYWRtaW4tYmFyXCIpKSB7XG4gICAgICAgICAgICBpZiAoalF1ZXJ5KFwiLm5hdmJhclwiKS5vZmZzZXQoKS50b3AgPiA1NSkge1xuICAgICAgICAgICAgICAgIGpRdWVyeShcIi5uYXZiYXItZml4ZWQtdG9wXCIpLmFkZENsYXNzKFwidG9wLW5hdi1jb2xsYXBzZVwiKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgalF1ZXJ5KFwiLm5hdmJhci1maXhlZC10b3BcIikucmVtb3ZlQ2xhc3MoXCJ0b3AtbmF2LWNvbGxhcHNlXCIpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgaWYgKGpRdWVyeShcIi5uYXZiYXJcIikub2Zmc2V0KCkudG9wID4gNTApIHtcbiAgICAgICAgICAgICAgICBqUXVlcnkoXCIubmF2YmFyLWZpeGVkLXRvcFwiKS5hZGRDbGFzcyhcInRvcC1uYXYtY29sbGFwc2VcIik7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGpRdWVyeShcIi5uYXZiYXItZml4ZWQtdG9wXCIpLnJlbW92ZUNsYXNzKFwidG9wLW5hdi1jb2xsYXBzZVwiKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH0pO1xuXG59KTtcblxuLy9HT09HTEUgTUFQXG5mdW5jdGlvbiBpbml0X21hcCgpIHtcbiAgICB2YXIgbXlPcHRpb25zID0ge1xuICAgICAgICB6b29tOiAxNyxcbiAgICAgICAgY2VudGVyOiBuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKDQ4LjYxMjIyMSwgNy43MjkxMTgpLCAvL2NoYW5nZSB0aGUgY29vcmRpbmF0ZXNcbiAgICAgICAgbWFwVHlwZUlkOiBnb29nbGUubWFwcy5NYXBUeXBlSWQuUk9BRE1BUCxcbiAgICAgICAgc2Nyb2xsd2hlZWw6IGZhbHNlLFxuICAgICAgICBzdHlsZXM6IFt7XG4gICAgICAgICAgICBmZWF0dXJlVHlwZTogJ2FsbCcsXG4gICAgICAgICAgICBzdHlsZXJzOiBbe1xuICAgICAgICAgICAgICAgIHNhdHVyYXRpb246IC0xMDBcbiAgICAgICAgICAgIH0sIHtcbiAgICAgICAgICAgICAgICBnYW1tYTogMC41MFxuICAgICAgICAgICAgfV1cbiAgICAgICAgfV1cbiAgICB9O1xuICAgIG1hcCA9IG5ldyBnb29nbGUubWFwcy5NYXAoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJnbWFwX2NhbnZhc1wiKSwgbXlPcHRpb25zKTtcbiAgICBtYXJrZXIgPSBuZXcgZ29vZ2xlLm1hcHMuTWFya2VyKHtcbiAgICAgICAgbWFwOiBtYXAsXG4gICAgICAgIHBvc2l0aW9uOiBuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKDQ4LjYxMjIyMSwgNy43MjkxMTgpIC8vY2hhbmdlIHRoZSBjb29yZGluYXRlc1xuICAgIH0pO1xufVxuZ29vZ2xlLm1hcHMuZXZlbnQuYWRkRG9tTGlzdGVuZXIod2luZG93LCAnbG9hZCcsIGluaXRfbWFwKTsiXX0=
