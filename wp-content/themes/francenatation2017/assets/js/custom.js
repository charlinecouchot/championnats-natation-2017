/**
 * scripts.js
 */

/*jshint unused:false,undef:false,strict:false */


jQuery(document).ready(function (jQuery) {
    'use strict';
    
    var PriorityNav = {

  // used multiple times, so saving.
  menu: jQuery(".navbar-nav"),
  allNavElements: jQuery(".navbar-nav > li:not('.navbar-overflow')"),

  init: function() {
    this.bindUIActions();
    this.setupMenu();
  },

  setupMenu: function() {
    
    // Checking top position of first item (sometimes changes)
    var firstPos = jQuery(".navbar-nav > li:first").position();
        
    // Empty collection in which to put menu items to move
    var wrappedElements = jQuery();

    // Used to snag the previous menu item in addition to ones that have wrapped
    var first = true;

    // Loop through all the nav items...
    PriorityNav.allNavElements.each(function(i) {
      
      var el = jQuery(this);

      // ...in which to find wrapped elements
      var pos = el.position();
                  
      if (pos.top !== firstPos.top) {

        // If element is wrapped, add it to set
        wrappedElements = wrappedElements.add(el);

        // Add the previous one too, if first
        /*if (first) {
          wrappedElements = wrappedElements.add(PriorityNav.allNavElements.eq(i-1));
          first = false;
        }*/
      }
    });
        
    if (wrappedElements.length) {
    
      // Clone set before altering
      var newSet = wrappedElements.clone();

      // Hide ones that we're moving
      wrappedElements.addClass("navbar-overflow__hidden");

      // Add wrapped elements to dropdown
      jQuery(".navbar-overflow__list").append(newSet);

      // Show new menu
      jQuery(".navbar-overflow").addClass("show-inline-block");

      // Make overflow visible again so dropdown can be seen.
      jQuery(".navbar-nav").css("overflow", "visible");

    }
  },

  tearDown: function() {
    jQuery(".navbar-overflow__list").empty();
    jQuery(".navbar-overflow").removeClass("show-inline-block");
    PriorityNav.allNavElements.removeClass("navbar-overflow__hidden");
  },

  bindUIActions: function() {
    jQuery(".navbar-overflow__button").on("click", function() {
      jQuery(".navbar-overflow__list").toggleClass("navbar-overflow__visible");
    });

    jQuery(window)
      .resize(function() {
        PriorityNav.menu.addClass("resizing");
      })
      .resize(_.debounce(function() {
        PriorityNav.tearDown();
        PriorityNav.setupMenu();
        PriorityNav.menu.removeClass("resizing");
      }, 500));
  }

};

PriorityNav.init();

    //SMOOTH SCROLL
    smoothScroll.init({
        selectorHeader: '[data-scroll-header]'
    });


    //COUNTDOWN TIMER
    jQuery('#countdown').countdown({
        until: new Date(2017, 5, 23),
        
    }); // enter event day

    //MILESTONE
    jQuery('.timer').countTo();


    jQuery('.register').magnificPopup({
        type: 'ajax'
    });

    //OWLCAROUSEL SCHEDULE
    var timetable = jQuery(".program__program-grid");
    var days = jQuery(".program__days");

    timetable.owlCarousel({
        items: 2,
        itemsMobile: [479, 1],
        slideSpeed: 1000,
        navigation: false,
        pagination: false,
        afterAction: syncPosition,
        responsiveRefreshRate: 200,
    });

    days.owlCarousel({
        items: 6,
        itemsMobile: [479, 4],
        pagination: false,
        responsiveRefreshRate: 100,
        afterInit: function (el) {
            el.find(".owl-item").eq(0).addClass("synced");
            el.find(".owl-item").eq(1).addClass("synced");
        }
    });

    function syncPosition(el) {
        var current = [this.currentItem, this.currentItem + 1];
        days.find(".owl-item").removeClass("synced").filter(function(index) {
            return current.indexOf(index) > -1;
        }).addClass("synced");
        if (days.data("owlCarousel") !== undefined) {
            center(current);
        }
    }

    days.on("click", ".owl-item", function (e) {
        e.preventDefault();
        var number = jQuery(this).data("owlItem");
        timetable.trigger("owl.goTo", number);
    });

    function center(number) {
        var daysvisible = days.data("owlCarousel").owl.visibleItems;
        var num = number;
        var found = false;
        for (var i in daysvisible) {
            if (num === daysvisible[i]) {
                found = true;
            }
        }

        if (found === false) {
            if (num > daysvisible[daysvisible.length - 1]) {
                days.trigger("owl.goTo", num - daysvisible.length + 2);
            } else {
                if (num - 1 === -1) {
                    num = 0;
                }
                days.trigger("owl.goTo", num);
            }
        } else if (num === daysvisible[daysvisible.length - 1]) {
            days.trigger("owl.goTo", daysvisible[1]);
        } else if (num === daysvisible[0]) {
            days.trigger("owl.goTo", num - 1);
        }

    }

    //FIX HOVER EFFECT ON IOS DEVICES
    document.addEventListener("touchstart", function () {}, true);
});

jQuery(window).load(function () {
    
    
    //PARALLAX BACKGROUND
    jQuery(window).resize(function() {
        var windowHeight = jQuery(this).height();
        jQuery('#splash').css('height', windowHeight);
    }).resize();
    
    jQuery(window).stellar({
        horizontalScrolling: false,
    });


    //PRELOADER
    jQuery('#preload').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.


    //HEADER ANIMATION
    jQuery(window).scroll(function () {
        if (jQuery("body").hasClass("admin-bar")) {
            if (jQuery(".navbar").offset().top > 55) {
                jQuery(".navbar-fixed-top").addClass("top-nav-collapse");
            } else {
                jQuery(".navbar-fixed-top").removeClass("top-nav-collapse");
            }
        } else {
            if (jQuery(".navbar").offset().top > 50) {
                jQuery(".navbar-fixed-top").addClass("top-nav-collapse");
            } else {
                jQuery(".navbar-fixed-top").removeClass("top-nav-collapse");
            }
        }
    });

});

//GOOGLE MAP
function init_map() {
    var myOptions = {
        zoom: 17,
        center: new google.maps.LatLng(48.612221, 7.729118), //change the coordinates
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false,
        styles: [{
            featureType: 'all',
            stylers: [{
                saturation: -100
            }, {
                gamma: 0.50
            }]
        }]
    };
    map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions);
    marker = new google.maps.Marker({
        map: map,
        position: new google.maps.LatLng(48.612221, 7.729118) //change the coordinates
    });
}
google.maps.event.addDomListener(window, 'load', init_map);